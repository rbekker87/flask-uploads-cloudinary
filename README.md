## Flask-Uploads-Cloudinary

<br>

Needed a way to share pictures and text files, so I wrote this piece of code that allows you to upload your file to Cloudinary.

<br>

* Runs on Flask Framework
* Saves the file to `/tmp/`
* Submits the file to Cloudinary
* Grabs the URL
* Deletes the local file from `/tmp/`
* Displays the URL where the content can be accessed from 

