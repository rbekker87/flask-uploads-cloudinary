# http://flask-caching.readthedocs.io/en/latest/

from flask import Flask, Response, request, stream_with_context, render_template, send_file
from flask_caching import Cache
import requests
import time

app = Flask(__name__)
#app.config['CACHE_TYPE'] = 'simple'
app.config['CACHE_TYPE'] = 'filesystem'
app.config['CACHE_DIR'] = 'cache'
app.config['CACHE_DEFAULT_TIMEOUT'] = 10

app.cache = Cache(app)


@app.route('/')
@app.cache.cached(timeout=10)
def index():
    url = 'https://res.cloudinary.com/rbekker/image/upload/v1528236191/gufzd6euht5dleietb5c.jpg'
    r = requests.get(url, stream = True)
    return Response(stream_with_context(r.iter_content()), content_type = r.headers['content-type'])

if __name__ == '__main__':
    app.run(port=809)