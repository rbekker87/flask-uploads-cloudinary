#!/usr/bin/env python

import urllib3
import sys
import cloudinary
import cloudinary.uploader
import cloudinary.api

urllib3.disable_warnings()

cloudinary.config(
    cloud_name = "xxxx",
    api_key = "xxxx",
    api_secret = "xxxx"
    )

file_object = sys.argv[1]

#res = cloudinary.uploader.upload(file_object)
res = cloudinary.uploader.upload_large(file_object, resource_type = "auto")
print('Upload URL: ' + res['secure_url'])

