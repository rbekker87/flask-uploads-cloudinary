from flask import Flask, render_template, request
from werkzeug import secure_filename
import requests
import urllib3
import sys
import os
import cloudinary
import cloudinary.uploader
import cloudinary.api

urllib3.disable_warnings()

cloudinary.config(
    cloud_name = "xxxx",
    api_key = "xxxx",
    api_secret = "xxxx"
    )

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = '/tmp/'

@app.route('/upload')
def upload_file():
   return render_template('upload.html')

@app.route('/uploader', methods = ['GET', 'POST'])
def uploader_file():
   if request.method == 'POST':
      f = request.files['file']
      filename = os.path.join(app.config['UPLOAD_FOLDER'], secure_filename(f.filename))
      f.save(filename)

      res = cloudinary.uploader.upload_large(filename, resource_type = "auto")
      genurl = res['secure_url']
      os.remove(filename)

      return '<html>Upload URL: ' + '<a href=' + genurl + '>' + genurl + '</a></html>'

if __name__ == '__main__':
   app.run(host='0.0.0.0',port=5080,debug = True)

